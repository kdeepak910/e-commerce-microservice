package com.dks.authentication.service;

import com.dks.authentication.entity.RoleEntity;
import com.dks.authentication.entity.UserDetailsEntity;
import com.dks.authentication.entity.UserEntity;
import com.dks.authentication.model.Role;
import com.dks.authentication.model.UserRequest;
import com.dks.authentication.model.UserResponse;
import com.dks.authentication.repository.RoleRepository;
import com.dks.authentication.repository.UserDetailsRepository;
import com.dks.authentication.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    private final String EMAIL = "deepak@gmail.com";
    private final String USER_NAME = "test";

    private final Long USER_ID = 2L;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    UserDetailsRepository userDetailsRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl userService;



    @Test
    public void get_all_users_test(){
        UserRequest userRequest = UserRequest.builder().username("deepak").email(EMAIL)
                .mobile("9642816601").build();
        RoleEntity roleEntity = RoleEntity.builder().name(Role.ROLE_USER).build();
        Set<RoleEntity> roleEntities = new HashSet<>();
        roleEntities.add(roleEntity);
        UserDetailsEntity userDetailsEntity = UserDetailsEntity.builder().firstName("Deepak").lastName("Sabat").build();
        UserEntity userEntity = UserEntity.builder().id(USER_ID).email(EMAIL).username(USER_NAME).userDetails(userDetailsEntity).roles(roleEntities).build();
        List<UserEntity> userEntities= new ArrayList<>();
        userEntities.add(userEntity);
        List<String> roles = new ArrayList<>();
        roles.add(String.valueOf(roleEntity));
        UserResponse userResponse = UserResponse.builder().email(EMAIL).username(USER_NAME).roles(roles).build();
        // given
        when(userRepository.findAll()).thenReturn(userEntities);
        List<UserResponse> foundUsers = userService.getAllUsers();
        // then
        assertEquals(foundUsers.get(0).getUsername(), USER_NAME);
        Mockito.verify(userRepository, times(1)).findAll();
        Mockito.verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void get_user_by_id_test(){
        RoleEntity roleEntity = RoleEntity.builder().name(Role.ROLE_USER).build();
        Set<RoleEntity> roleEntities = new HashSet<>();
        roleEntities.add(roleEntity);
        UserDetailsEntity userDetailsEntity = UserDetailsEntity.builder().firstName("Deepak").lastName("Sabat").build();
        UserEntity userEntity = UserEntity.builder().id(USER_ID).email(EMAIL).username(USER_NAME).roles(roleEntities).userDetails(userDetailsEntity).build();
        // given
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(userEntity));
        // when
        UserResponse foundUser = userService.getUserById(USER_ID);
        // then
        assertEquals(foundUser.getUsername(), USER_NAME);
        Mockito.verify(userRepository, times(1)).findById(anyLong());
        Mockito.verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void get_user_by_name_test(){
        RoleEntity roleEntity = RoleEntity.builder().name(Role.ROLE_USER).build();
        Set<RoleEntity> roleEntities = new HashSet<>();
        roleEntities.add(roleEntity);
        UserDetailsEntity userDetailsEntity = UserDetailsEntity.builder().firstName("Deepak").lastName("Sabat").build();
        UserEntity userEntity = UserEntity.builder().id(USER_ID).email(EMAIL).username(USER_NAME).roles(roleEntities).userDetails(userDetailsEntity).build();
        // given
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(userEntity));
        // when
        UserResponse foundUser = userService.getUserByName(USER_NAME);
        // then
        assertEquals(foundUser.getUsername(), USER_NAME);
        Mockito.verify(userRepository, times(1)).findByUsername(USER_NAME);
        Mockito.verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void delete_user_test() throws Exception{
        RoleEntity roleEntity = RoleEntity.builder().name(Role.ROLE_USER).build();
        Set<RoleEntity> roleEntities = new HashSet<>();
        roleEntities.add(roleEntity);
        UserDetailsEntity userDetailsEntity = UserDetailsEntity.builder().firstName("Deepak").lastName("Sabat").build();
        UserEntity userEntity = UserEntity.builder().id(USER_ID).email(EMAIL).username(USER_NAME).roles(roleEntities).userDetails(userDetailsEntity).build();
        // given
        Mockito.when(userRepository.findById(USER_ID)).thenReturn(Optional.of(userEntity));
        userService.deleteUser(USER_ID);
        Mockito.verify(userRepository, times(1)).save(userEntity);

    }

    @Test
    public void create_user_test() throws  Exception {
        UserRequest userRequest = UserRequest.builder().username(USER_NAME).email(EMAIL)
                .mobile("9642816601").firstName("Deepak").lastName("Sabat").build();
        RoleEntity roleEntity = RoleEntity.builder().name(Role.ROLE_USER).build();
        Set<RoleEntity> roleEntities = new HashSet<>();
        roleEntities.add(roleEntity);
        UserDetailsEntity userDetailsEntity = UserDetailsEntity.builder().firstName("Deepak").lastName("Sabat").build();
        UserEntity userEntity = UserEntity.builder().email(EMAIL).mobile("9642816601").active(1).username(USER_NAME).roles(roleEntities).userDetails(userDetailsEntity).build();
        Mockito.when(roleRepository.findByName(Role.ROLE_USER)).thenReturn(Optional.of(roleEntity));
        Mockito.when(passwordEncoder.encode("Test123")).thenReturn(anyString());
        userService.createUser(userRequest);
        Mockito.verify(userRepository, times(1)).save(userEntity);
    }

    @Test
    public void update_user_test() throws Exception {
        UserRequest userRequest = UserRequest.builder().username(USER_NAME).email(EMAIL)
                .mobile("9642816601").firstName("Deepak").lastName("Sabat").build();
        RoleEntity roleEntity = RoleEntity.builder().name(Role.ROLE_USER).build();
        Set<RoleEntity> roleEntities = new HashSet<>();
        roleEntities.add(roleEntity);
        UserDetailsEntity userDetailsEntity = UserDetailsEntity.builder().firstName("Deepak").lastName("Sabat").build();
        UserEntity userEntity = UserEntity.builder().email(EMAIL).mobile("9642816601").active(1).username(USER_NAME).roles(roleEntities).userDetails(userDetailsEntity).build();
        Mockito.when(userRepository.findById(1L)).thenReturn(Optional.of(userEntity));
        Mockito.when(userDetailsRepository.findById(1L)).thenReturn(Optional.of(userDetailsEntity));
        userService.updateUser(userRequest, 1L);
        Mockito.verify(userRepository, times(1)).saveAndFlush(userEntity);

    }
}
