package com.dks.authentication.controller;


import com.dks.authentication.model.JwtResponse;
import com.dks.authentication.model.LoginRequest;
import com.dks.authentication.model.UserRequest;
import com.dks.authentication.service.UserInfoUserDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    AuthenticationManager authenticationManager;

    private final String EMAIL = "deepak@gmail.com";
    private final String PASSWORD = "Test123";
    private final String USER_NAME = "test";

    private final Long USER_ID = 2L;




    @Test
    public void loginSuccess() throws Exception{
        LoginRequest loginRequest = LoginRequest.builder().email(EMAIL).password(PASSWORD).build();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter objectWriter = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = objectWriter.writeValueAsString(loginRequest);
        UserInfoUserDetails userInfoUserDetails = new UserInfoUserDetails();
        userInfoUserDetails.setId(USER_ID);
        userInfoUserDetails.setEmail(EMAIL);
        userInfoUserDetails.setUsername(USER_NAME);
        userInfoUserDetails.setPassword(PASSWORD);

        //when
        when(authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(EMAIL, PASSWORD)).getPrincipal()).thenReturn(userInfoUserDetails);
        //then
        mockMvc.perform(post("/api/auth/login").content(requestJson).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].email").value(EMAIL))
                .andExpect(jsonPath("$[0].username").value(USER_NAME));

        verify(authenticationManager, times(1)).authenticate(any(UsernamePasswordAuthenticationToken.class));
        verifyNoMoreInteractions(authenticationManager);
    }
}
