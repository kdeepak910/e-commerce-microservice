package com.dks.authentication.controller;

import com.dks.authentication.entity.UserEntity;
import com.dks.authentication.model.UserRequest;
import com.dks.authentication.model.UserResponse;
import com.dks.authentication.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    private final String EMAIL = "deepak@gmail.com";
    private final String USER_NAME = "test";

    private final Long USER_ID = 2L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserService userService;

    @Test
    public void get_all_users_controller_should_return200_when_validRequest() throws Exception{
        //given
        UserResponse userResponse = new UserResponse();
        userResponse.setEmail(EMAIL);
        userResponse.setUsername(USER_NAME);
        List<UserResponse> users= new ArrayList<>();
        users.add(userResponse);

        //when
        when(userService.getAllUsers()).thenReturn(users);

        //then
        mockMvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].email").value(EMAIL))
                .andExpect(jsonPath("$[0].username").value(USER_NAME));

        verify(userService, times(1)).getAllUsers();
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void get_all_users_controller_should_return404_when_userList_isEmpty() throws Exception{
        //given
        List<UserResponse> users = new ArrayList<UserResponse>();
        //when
        when(userService.getAllUsers()).thenReturn(users);

        //then
        mockMvc.perform(get("/api/users"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE));

        verify(userService, times(1)).getAllUsers();
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void get_user_by_name_controller_should_return200_when_users_isExist() throws Exception{
        //given
        UserResponse userResponse = new UserResponse();
        userResponse.setEmail(EMAIL);
        userResponse.setUsername(USER_NAME);

        //when
        when(userService.getUserByName(USER_NAME)).thenReturn(userResponse);

        //then
        mockMvc.perform(get("/api/users/find/"+USER_NAME))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.email").value(EMAIL))
                .andExpect(jsonPath("$.username").value(USER_NAME));

        verify(userService, times(1)).getUserByName(anyString());
        verifyNoMoreInteractions(userService);
    }
    @Test
    public void get_user_by_name_controller_should_return404_when_users_is_notExist() throws Exception{

        //when
        when(userService.getUserByName(USER_NAME)).thenReturn(null);

        //then
        mockMvc.perform(get("/api/users/find/"+USER_NAME))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE));

        verify(userService, times(1)).getUserByName(anyString());
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void get_user_by_id_controller_should_return200_when_users_isExist() throws Exception{
        //given
        UserResponse userResponse = new UserResponse();
        userResponse.setEmail(EMAIL);
        userResponse.setUsername(USER_NAME);


        //when
        when(userService.getUserById(USER_ID)).thenReturn(userResponse);

        //then
        mockMvc.perform(get("/api/users/"+USER_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.email").value(EMAIL))
                .andExpect(jsonPath("$.username").value(USER_NAME));

        verify(userService, times(1)).getUserById(anyLong());
        verifyNoMoreInteractions(userService);
    }
    @Test
    public void get_user_by_id_controller_should_return404_when_users_is_notExist() throws Exception{

        //when
        when(userService.getUserById(USER_ID)).thenReturn(null);

        //then
        mockMvc.perform(get("/api/users/"+USER_ID))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE));

        verify(userService, times(1)).getUserById(anyLong());
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void add_user_controller_should_return201_when_user_is_saved() throws Exception{
        //given
        UserRequest userRequest = UserRequest.builder().username("deepak").email(EMAIL)
                .mobile("9642816601").build();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter objectWriter = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = objectWriter.writeValueAsString(userRequest);

        //when
        doNothing().when(userService).createUser(userRequest);

        //then
        mockMvc.perform(post("/api/users/create").content(requestJson).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        verify(userService, times(1)).createUser(any(UserRequest.class));
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void update_user_controller_should_return201_when_user_is_saved() throws Exception{
        //given
        UserRequest userRequest = UserRequest.builder().username("deepak").email(EMAIL)
                .mobile("9642816601").build();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter objectWriter = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = objectWriter.writeValueAsString(userRequest);

        //when
        doNothing().when(userService).updateUser(userRequest, USER_ID);

        //then
        mockMvc.perform(post("/api/users/update/"+USER_ID).content(requestJson).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        verify(userService, times(1)).updateUser(any(UserRequest.class), anyLong());
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void delete_user_by_id_controller_should_return200_when_users_isExist() throws Exception{

        doNothing().when(userService).deleteUser(USER_ID);
        mockMvc.perform(post("/api/users/delete/"+USER_ID))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        verify(userService, times(1)).deleteUser(anyLong());
        verifyNoMoreInteractions(userService);
    }
}
