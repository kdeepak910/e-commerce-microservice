package com.dks.authentication.controller;

import com.dks.authentication.http.HeaderGenerator;
import com.dks.authentication.model.UserRequest;
import com.dks.authentication.model.UserResponse;
import com.dks.authentication.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    HeaderGenerator headerGenerator;


    @GetMapping (value = "/users")
    public ResponseEntity<List<UserResponse>> getAllUsers(){
        List<UserResponse> users =  userService.getAllUsers();
        if(!users.isEmpty()) {
            return new ResponseEntity<>(
                    users,
                    headerGenerator.getHeadersForSuccessGetMethod(),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                headerGenerator.getHeadersForError(),
                HttpStatus.NOT_FOUND);
    }

    @GetMapping (value = "/users/find/{name}")
    public ResponseEntity<UserResponse> getUserByName(@PathVariable("name") String userName){
        UserResponse user = userService.getUserByName(userName);
        if(Optional.ofNullable(user).isPresent()) {
            return new ResponseEntity<>(
                    user,
                    headerGenerator.
                            getHeadersForSuccessGetMethod(),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                headerGenerator.getHeadersForError(),
                HttpStatus.NOT_FOUND);
    }

    @GetMapping (value = "/users/{id}")
    public ResponseEntity<UserResponse> getUserById(@PathVariable("id") Long id){
        UserResponse user = userService.getUserById(id);
        if(Optional.ofNullable(user).isPresent()) {
            return new ResponseEntity<>(
                    user,
                    headerGenerator.
                            getHeadersForSuccessGetMethod(),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                headerGenerator.getHeadersForError(),
                HttpStatus.NOT_FOUND);
    }

    @PostMapping("/users/create")
    public ResponseEntity addUser(@RequestBody UserRequest userRequest, HttpServletRequest request){
        if(Optional.ofNullable(userRequest).isPresent()) {
            try {
                userService.createUser(userRequest);
                return new ResponseEntity<>(
                        headerGenerator.getHeadersForSuccessGetMethod(),
                        HttpStatus.CREATED);
            }catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Request is not valid");
    }

    @PostMapping("/users/update/{id}")
    public ResponseEntity updateUser(@RequestBody UserRequest userRequest, @PathVariable Long id){
        if(Optional.ofNullable(userRequest).isPresent()) {
            try {
                userService.updateUser(userRequest, id);
                return new ResponseEntity<>(
                        headerGenerator.getHeadersForSuccessGetMethod(),
                        HttpStatus.CREATED);
            }catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Request is not valid");
    }

    @PostMapping("/users/delete/{id}")
    public ResponseEntity deleteUser(@PathVariable Long id){
        try {
            userService.deleteUser(id);
            return new ResponseEntity<>(
                    headerGenerator.getHeadersForSuccessGetMethod(),
                    HttpStatus.CREATED);
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
