package com.dks.authentication.model;

public enum Role {

    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
