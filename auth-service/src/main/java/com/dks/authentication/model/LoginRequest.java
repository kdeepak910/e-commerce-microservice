package com.dks.authentication.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginRequest implements Serializable {

    private static final long serialVersionUID = 10L;
    private String email;
    private String password;

}
