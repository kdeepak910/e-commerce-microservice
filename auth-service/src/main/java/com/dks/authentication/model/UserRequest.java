package com.dks.authentication.model;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserRequest implements Serializable {

    private static final long serialVersionUID = 10L;

    private String username;
    private String email;
    private String password;
    private String mobile;
    private String firstName;
    private String lastName;
    private String alternativeMobile;
    private String address;
    private String zipCode;
    private String city;
    private String country;

}
