package com.dks.authentication.constant;

public class StringConstants {

    public static final String ROLE_ERROR_MESSAGE = "Error: Role is not found.";
    public static final String HEADER_TYPE = "Content-Type";

    public static final String CODE = "201";

    public StringConstants() {
    }
}
