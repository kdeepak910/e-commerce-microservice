package com.dks.authentication.repository;

import com.dks.authentication.entity.RoleEntity;
import com.dks.authentication.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {

    Optional <RoleEntity> findByName(Role name);
}
