package com.dks.authentication.service;

import com.dks.authentication.model.UserRequest;
import com.dks.authentication.model.UserResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    List<UserResponse> getAllUsers();
    UserResponse getUserById(Long id);
    UserResponse getUserByName(String userName);
    void createUser(UserRequest userRequest);

    void updateUser(UserRequest userRequest, Long id);

    void deleteUser(Long id);
}
