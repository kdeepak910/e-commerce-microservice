package com.dks.authentication.service;

import com.dks.authentication.entity.RoleEntity;
import com.dks.authentication.entity.UserDetailsEntity;
import com.dks.authentication.entity.UserEntity;
import com.dks.authentication.model.Role;
import com.dks.authentication.model.UserRequest;
import com.dks.authentication.model.UserResponse;
import com.dks.authentication.repository.RoleRepository;
import com.dks.authentication.repository.UserDetailsRepository;
import com.dks.authentication.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserDetailsRepository userDetailsRepository;


    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public List<UserResponse> getAllUsers() {
        List<UserResponse> userResponses = new ArrayList<>();
        List<UserEntity> userEntities = userRepository.findAll();
        userEntities.forEach(userEntity -> userResponses.add(userResponseBuilder(userEntity)));
        return userResponses;
    }

    @Override
    public UserResponse getUserById(Long id) {
        Optional<UserEntity> user = userRepository.findById(id);
        return user.isPresent() ? userResponseBuilder(user.get()) : UserResponse.builder().build();
    }

    @Override
    public UserResponse getUserByName(String userName) {
        Optional<UserEntity> user =  userRepository.findByUsername(userName);
        return user.isPresent() ? userResponseBuilder(user.get()) : UserResponse.builder().build();
    }

    @Override
    @Transactional
    public void createUser(UserRequest userRequest) {
        Set<RoleEntity> roles = new HashSet<>();
        RoleEntity userRole = roleRepository.findByName(Role.ROLE_USER).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        roles.add(userRole);
        UserDetailsEntity userDetailsEntity = UserDetailsEntity.builder().firstName(userRequest.getFirstName())
                .lastName(userRequest.getLastName()).alternativeMobile(userRequest.getAlternativeMobile())
                .address(userRequest.getAddress()).city(userRequest.getCity()).country(userRequest.getCountry())
                .zipCode(userRequest.getZipCode()).build();
        UserEntity user = UserEntity.builder().username(userRequest.getUsername()).email(userRequest.getEmail())
                .password(passwordEncoder.encode(userRequest.getPassword())).mobile(userRequest.getMobile())
                .roles(roles).userDetails(userDetailsEntity).active(1).build();
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updateUser(UserRequest userRequest, Long id) {
        UserEntity user = userRepository.findById(id).get();
        UserDetailsEntity userDetailsEntity = userDetailsRepository.findById(user.getUserDetails().getId()).get();
        if(StringUtils.isNotEmpty(userDetailsEntity.getFirstName())) {
            userDetailsEntity.setFirstName(userRequest.getFirstName());
            userDetailsEntity.setLastName(userRequest.getFirstName());
            userDetailsEntity.setCity(userRequest.getCity());
            userDetailsEntity.setAddress(userRequest.getAddress());
            userDetailsEntity.setCountry(userRequest.getCountry());
            userDetailsEntity.setZipCode(userRequest.getZipCode());
            userDetailsEntity.setAlternativeMobile(userRequest.getAlternativeMobile());
        }

        if(StringUtils.isNotEmpty(user.getUsername())) {
            user.setUserDetails(userDetailsEntity);
            user.setMobile(userRequest.getMobile());
            user.setEmail(userRequest.getEmail());
            user.setUsername(userRequest.getUsername());
            user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        }
        userRepository.saveAndFlush(user);
    }

    @Override
    @Transactional
    public void deleteUser(Long id) {
        UserEntity user = userRepository.findById(id).get();
        if(Optional.ofNullable(user).isPresent()) {
            user.setActive(0);
            userRepository.save(user);
        }
    }

    public UserResponse userResponseBuilder(UserEntity userEntity) {
        List<String> stringList = userEntity.getRoles().stream()
                .map(role -> role.getName().name())
                .toList();
        return UserResponse.builder().username(userEntity.getUsername()).email(userEntity.getEmail())
                .mobile(userEntity.getMobile()).firstName(userEntity.getUserDetails().getFirstName())
                .lastName(userEntity.getUserDetails().getLastName()).alternativeMobile(userEntity.getUserDetails().getAlternativeMobile())
                .address(userEntity.getUserDetails().getAddress()).city(userEntity.getUserDetails().getCity())
                .zipCode(userEntity.getUserDetails().getZipCode()).country(userEntity.getUserDetails().getCountry())
                .roles(stringList).build();
    }
}
