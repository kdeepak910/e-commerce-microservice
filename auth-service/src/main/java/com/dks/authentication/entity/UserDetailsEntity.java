package com.dks.authentication.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "user_details")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UserDetailsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "first_name", nullable = false, length = 50)
    private String firstName;
    @Column (name = "last_name", nullable = false, length = 50)
    private String lastName;
    @Column (name = "alternative_number")
    private String alternativeMobile;
    @Column (name = "address", length = 30)
    private String address;
    @Column (name = "zip_code", length = 6)
    private String zipCode;
    @Column (name = "city", length = 30)
    private String city;
    @Column (name = "country", length = 30)
    private String country;

    @OneToOne(mappedBy = "userDetails")
    @JsonIgnore
    private UserEntity user;


}
