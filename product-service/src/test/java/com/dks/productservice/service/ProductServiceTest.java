package com.dks.productservice.service;

import com.dks.productservice.entity.ProductEntity;
import com.dks.productservice.model.ProductRequest;
import com.dks.productservice.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {

    private static final String PRODUCT_NAME= "test";
    private static final Long PRODUCT_ID = 5L;
    private static final String PRODUCT_CATEGORY = "testCategory";

    private List<ProductEntity> products;
    private ProductEntity product;

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productService;

    @Before
    public void setUp(){
        product = new ProductEntity();
        product.setId(PRODUCT_ID);
        product.setProductName(PRODUCT_NAME);
        product.setCategory(PRODUCT_CATEGORY);
        products = new ArrayList<ProductEntity>();
        products.add(product);
    }

    @Test
    public void get_all_product_test(){
        // Data preparation
        String productName = "test";

        Mockito.when(productRepository.findAll()).thenReturn(products);

        // Method call
        List<ProductEntity> foundProducts = productService.getAllProduct();

        // Verification
        assertEquals(foundProducts.get(0).getProductName(), productName);
        Mockito.verify(productRepository, Mockito.times(1)).findAll();
        Mockito.verifyNoMoreInteractions(productRepository);
    }

    @Test
    public void get_one_by_id_test(){
        // Data preparation
        Mockito.when(productRepository.findById(PRODUCT_ID)).thenReturn(Optional.of(product));

        // Method call
        ProductEntity found = productService.getProductById(PRODUCT_ID);

        // Verification
        assertEquals(found.getId(), PRODUCT_ID);
        Mockito.verify(productRepository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verifyNoMoreInteractions(productRepository);
    }

    @Test
    public void get_all_product_by_category_test(){
        // Data preparation
        Mockito.when(productRepository.findAllByCategory(PRODUCT_CATEGORY)).thenReturn(products);

        //Method call
        List<ProductEntity> foundProducts = productService.getAllProductByCategory(PRODUCT_CATEGORY);

        //Verification
        assertEquals(products.get(0).getCategory(), PRODUCT_CATEGORY);
        assertEquals(products.get(0).getProductName(), PRODUCT_NAME);
        Mockito.verify(productRepository, Mockito.times(1)).findAllByCategory(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(productRepository);
    }

    @Test
    public void get_all_products_by_name_test(){
        // Data preparation
        Mockito.when(productRepository.findAllByProductName(PRODUCT_NAME)).thenReturn(products);

        //Method call
        List<ProductEntity> foundProducts = productService.getAllProductsByName(PRODUCT_NAME);

        //Verification
        assertEquals(foundProducts.get(0).getProductName(), PRODUCT_NAME);
        Mockito.verify(productRepository, Mockito.times(1)).findAllByProductName(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(productRepository);
    }

    @Test
    public void delete_product_test() throws Exception{

        // given
        productService.deleteProduct(PRODUCT_ID);
        Mockito.verify(productRepository, times(1)).deleteById(PRODUCT_ID);

    }

    @Test
    public void create_product_test() throws  Exception {
        ProductRequest productRequest = ProductRequest.builder().price(BigDecimal.ONE).description("Test").availability(1).productName(PRODUCT_NAME).category(PRODUCT_CATEGORY).build();
        Mockito.when(productRepository.save(product)).thenReturn(product);
        productService.createProduct(productRequest);

        Mockito.verify(productRepository, times(1)).save(any(ProductEntity.class));
    }

    @Test
    public void update_product_test() throws Exception {
        ProductRequest productRequest = ProductRequest.builder().productName(PRODUCT_NAME).category(PRODUCT_CATEGORY).build();
        Mockito.when(productRepository.findById(PRODUCT_ID)).thenReturn(Optional.of(product));
        productService.updateProduct(productRequest, PRODUCT_ID);
        Mockito.verify(productRepository, times(1)).saveAndFlush(product);

    }
}
