package com.dks.productservice.controller;

import com.dks.productservice.model.ProductRequest;
import com.dks.productservice.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AdminProductControllerTest {

    private static final String PRODUCT_NAME= "test";
    private static final String PRODUCT_CATEGORY = "testCategory";

    private static final Long PRODUCT_ID = 1L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Test
    public void add_product_controller_should_return201_when_product_isSaved() throws Exception {
        //given
        ProductRequest product = new ProductRequest();
        product.setProductName(PRODUCT_NAME);
        product.setCategory(PRODUCT_CATEGORY);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter objectWriter = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = objectWriter.writeValueAsString(product);

        //when
        doNothing().when(productService).createProduct(new ProductRequest());

        //then
        mockMvc.perform(post("/api/admin/product/create").content(requestJson).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        verify(productService, times(1)).createProduct(any(ProductRequest.class));
        verifyNoMoreInteractions(productService);
    }

    @Test
    public void add_product_controller_should_return400_when_product_isNull() throws Exception {
        //given
        ProductRequest product = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter objectWriter = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = objectWriter.writeValueAsString(product);

        //then
        mockMvc.perform(post("/api/admin/product/create").content(requestJson).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void update_user_controller_should_return201_when_user_is_saved() throws Exception{
        //given
        ProductRequest productRequest = ProductRequest.builder().productName(PRODUCT_NAME).category(PRODUCT_CATEGORY).build();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter objectWriter = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = objectWriter.writeValueAsString(productRequest);

        //when
        doNothing().when(productService).updateProduct(productRequest, PRODUCT_ID);

        //then
        mockMvc.perform(post("/api/admin/product/update/"+PRODUCT_ID).content(requestJson).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        verify(productService, times(1)).updateProduct(any(ProductRequest.class), anyLong());
        verifyNoMoreInteractions(productService);
    }

    @Test
    public void delete_product_by_id_controller_should_return200_when_users_isExist() throws Exception{

        doNothing().when(productService).deleteProduct(PRODUCT_ID);
        mockMvc.perform(post("/api/admin/product/delete/"+PRODUCT_ID))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        verify(productService, times(1)).deleteProduct(anyLong());
        verifyNoMoreInteractions(productService);
    }

}
