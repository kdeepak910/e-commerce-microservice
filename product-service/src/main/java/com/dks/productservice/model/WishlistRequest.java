package com.dks.productservice.model;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class WishlistRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long productId;
    private String productName;
    private Long userId;
    private String userName;
}
