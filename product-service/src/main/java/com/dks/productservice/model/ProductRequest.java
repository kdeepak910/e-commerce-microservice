package com.dks.productservice.model;


import jakarta.persistence.Column;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductRequest implements Serializable {

    private static final long serialVersionUID = 10L;
    private String productName;
    private BigDecimal price;
    private String description;
    private String category;
    private int availability;
}
