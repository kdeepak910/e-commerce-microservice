package com.dks.productservice.event.publisher;

import com.dks.productservice.messaging.MessageContainer;
import com.dks.productservice.model.WishlistRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class WishlistPublisher {

    @Value("${productservice.topic.name.producer}")
    private String topicName;

    private final KafkaTemplate<String, MessageContainer<WishlistRequest>> kafkaTemplate;

    public void publish(MessageContainer<WishlistRequest> messageContainer) {
        try {
            WishlistRequest wishlistRequest = messageContainer.getPayload();
            log.info("WishlistPublisher to publish back to recommend service to add wishList product details for product_id {} and product_name {}",
                    wishlistRequest.getProductId(), wishlistRequest.getProductName());
            kafkaTemplate.send(topicName, messageContainer);
        } catch (Exception e) {
            log.error("Exception while publishing WishlistPublisher for messageContainer {} exception {}", messageContainer,
                    e.toString());
            throw e;
        }
    }
}
