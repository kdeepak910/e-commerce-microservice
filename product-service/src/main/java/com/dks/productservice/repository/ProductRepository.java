package com.dks.productservice.repository;

import com.dks.productservice.entity.ProductEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Transactional
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    public List<ProductEntity> findAllByCategory(String category);
    public List<ProductEntity> findAllByProductName(String name);
}
