package com.dks.productservice.controller;

import com.dks.productservice.http.HeaderGenerator;
import com.dks.productservice.model.ProductRequest;
import com.dks.productservice.service.ProductService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/admin/")
public class AdminProductController {

    @Autowired
    ProductService productService;

    @Autowired
    HeaderGenerator headerGenerator;

    @PostMapping("/product/create")
    public ResponseEntity createProduct(@RequestBody ProductRequest productRequest, HttpServletRequest request){
        if(Optional.ofNullable(productRequest).isPresent()) {
            try {
                productService.createProduct(productRequest);
                return new ResponseEntity<>(
                        headerGenerator.getHeadersForSuccessGetMethod(),
                        HttpStatus.CREATED);
            }catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Request is not valid");
    }

    @PostMapping("/product/update/{id}")
    public ResponseEntity updateProduct(@RequestBody ProductRequest productRequest, @PathVariable Long id){
        if(Optional.ofNullable(productRequest).isPresent()) {
            try {
                productService.updateProduct(productRequest, id);
                return new ResponseEntity<>(
                        headerGenerator.getHeadersForSuccessGetMethod(),
                        HttpStatus.CREATED);
            }catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Request is not valid");
    }

    @PostMapping("/product/delete/{id}")
    public ResponseEntity deleteProduct(@PathVariable Long id){
        try {
            productService.deleteProduct(id);
            return new ResponseEntity<>(
                    headerGenerator.getHeadersForSuccessGetMethod(),
                    HttpStatus.CREATED);
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
