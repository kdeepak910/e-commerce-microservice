package com.dks.productservice.controller;

import com.dks.productservice.entity.ProductEntity;
import com.dks.productservice.http.HeaderGenerator;
import com.dks.productservice.model.WishlistRequest;
import com.dks.productservice.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ProductController {


    private final ProductService productService;
    private final HeaderGenerator headerGenerator;

    @GetMapping(value = "/products")
    public ResponseEntity<List<ProductEntity>> getAllProducts(){
        List<ProductEntity> products =  productService.getAllProduct();
        if(!products.isEmpty()) {
            return new ResponseEntity<List<ProductEntity>>(
                    products,
                    headerGenerator.getHeadersForSuccessGetMethod(),
                    HttpStatus.OK);
        }
        return new ResponseEntity<List<ProductEntity>>(
                headerGenerator.getHeadersForError(),
                HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/products", params = "category")
    public ResponseEntity<List<ProductEntity>> getAllProductByCategory(@RequestParam("category") String category){
        List<ProductEntity> products = productService.getAllProductByCategory(category);
        if(!products.isEmpty()) {
            return new ResponseEntity<List<ProductEntity>>(
                    products,
                    headerGenerator.getHeadersForSuccessGetMethod(),
                    HttpStatus.OK);
        }
        return new ResponseEntity<List<ProductEntity>>(
                headerGenerator.getHeadersForError(),
                HttpStatus.NOT_FOUND);
    }

    @GetMapping (value = "/products/{id}")
    public ResponseEntity<ProductEntity> getOneProductById(@PathVariable("id") long id){
        ProductEntity product =  productService.getProductById(id);
        if(product != null) {
            return new ResponseEntity<ProductEntity>(
                    product,
                    headerGenerator.getHeadersForSuccessGetMethod(),
                    HttpStatus.OK);
        }
        return new ResponseEntity<ProductEntity>(
                headerGenerator.getHeadersForError(),
                HttpStatus.NOT_FOUND);
    }

    @GetMapping (value = "/products", params = "name")
    public ResponseEntity<List<ProductEntity>> getAllProductsByName(@RequestParam ("name") String name){
        List<ProductEntity> products =  productService.getAllProductsByName(name);
        if(!products.isEmpty()) {
            return new ResponseEntity<List<ProductEntity>>(
                    products,
                    headerGenerator.getHeadersForSuccessGetMethod(),
                    HttpStatus.OK);
        }
        return new ResponseEntity<List<ProductEntity>>(
                headerGenerator.getHeadersForError(),
                HttpStatus.NOT_FOUND);
    }

    @PostMapping("/wishlist/add")
    public ResponseEntity addWishListProduct(@RequestBody WishlistRequest wishlistRequest) {
        if(Optional.ofNullable(wishlistRequest).isPresent()) {
            try {
                productService.createWishListProduct(wishlistRequest);
                return new ResponseEntity<>(
                        headerGenerator.getHeadersForSuccessGetMethod(),
                        HttpStatus.CREATED);
            }catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Request is not valid");
    }
}
