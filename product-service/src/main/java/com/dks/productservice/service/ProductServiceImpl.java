package com.dks.productservice.service;

import com.dks.productservice.entity.ProductEntity;
import com.dks.productservice.event.publisher.WishlistPublisher;
import com.dks.productservice.messaging.MessageContainer;
import com.dks.productservice.messaging.MessageContainerUtil;
import com.dks.productservice.model.ProductRequest;
import com.dks.productservice.model.WishlistRequest;
import com.dks.productservice.repository.ProductRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@Transactional
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    WishlistPublisher wishlistPublisher;

    @Override
    public List<ProductEntity> getAllProduct() {
        return productRepository.findAll();
    }

    @Override
    public List<ProductEntity> getAllProductByCategory(String category) {
        return productRepository.findAllByCategory(category);
    }

    @Override
    public ProductEntity getProductById(Long id) {
        return productRepository.findById(id).get();
    }

    @Override
    public List<ProductEntity> getAllProductsByName(String name) {
        return productRepository.findAllByProductName(name);
    }
    @Override
    @Transactional
    public void createProduct(ProductRequest productRequest) {
        ProductEntity productEntity = ProductEntity.builder().productId(UUID.randomUUID()).productName(productRequest.getProductName())
                .price(productRequest.getPrice()).category(productRequest.getCategory()).discription(productRequest.getDescription())
                .availability(productRequest.getAvailability()).build();
        productRepository.save(productEntity);
    }

    @Override
    public void updateProduct(ProductRequest productRequest, Long id) {
        ProductEntity productEntity = productRepository.findById(id).get();
        if(Optional.ofNullable(productEntity).isPresent()) {
            productEntity.setProductName(productRequest.getProductName());
            productEntity.setCategory(productRequest.getCategory());
            productEntity.setDiscription(productRequest.getDescription());
            productEntity.setAvailability(productRequest.getAvailability());
            productEntity.setPrice(productRequest.getPrice());
        }
        productRepository.saveAndFlush(productEntity);
    }

    @Override
    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public void createWishListProduct(WishlistRequest wishlistRequest) {
        try {
            MessageContainer<WishlistRequest> mcBase = MessageContainerUtil.getPayload(wishlistRequest);
            wishlistPublisher.publish(mcBase);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
