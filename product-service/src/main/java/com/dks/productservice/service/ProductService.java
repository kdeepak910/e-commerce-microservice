package com.dks.productservice.service;

import com.dks.productservice.entity.ProductEntity;
import com.dks.productservice.model.ProductRequest;
import com.dks.productservice.model.WishlistRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {

    public List<ProductEntity> getAllProduct();
    public List<ProductEntity> getAllProductByCategory(String category);
    public ProductEntity getProductById(Long id);
    public List<ProductEntity> getAllProductsByName(String name);

    void createProduct(ProductRequest productRequest);

    void updateProduct(ProductRequest productRequest, Long id);

    void deleteProduct(Long id);

    void createWishListProduct(WishlistRequest wishlistRequest);
}
