package com.dks.recommendationservice.client;

import com.dks.recommendationservice.model.UserResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "UserResponse", url = "http://localhost:9191/")
public interface UserClient {
    @GetMapping(value = "/api/users/{id}")
    public UserResponse getUserById(@PathVariable("id") Long id);
}
