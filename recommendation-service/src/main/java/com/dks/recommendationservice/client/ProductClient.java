package com.dks.recommendationservice.client;

import com.dks.recommendationservice.entity.ProductEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "product-service", url = "http://localhost:9192/")
public interface ProductClient {
    @GetMapping(value = "api/products/{id}")
    public ProductEntity getProductById(@PathVariable(value = "id") Long productId);

}
