package com.dks.recommendationservice.service;

import com.dks.recommendationservice.entity.WishListEntity;
import com.dks.recommendationservice.model.WishlistRequest;
import org.springframework.stereotype.Service;

@Service
public interface WishlistService {

    WishListEntity getWishListProductById(Long productId);

    void createWishListProduct(WishlistRequest wishlistRequest);
}
