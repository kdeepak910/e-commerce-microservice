package com.dks.recommendationservice.service;

import com.dks.recommendationservice.entity.WishListEntity;
import com.dks.recommendationservice.model.WishlistRequest;
import com.dks.recommendationservice.repository.WishListRepository;
import org.springframework.stereotype.Service;

@Service
public class WishlistServiceImpl implements WishlistService{

    private WishListRepository wishListRepository;

    public WishlistServiceImpl(WishListRepository wishListRepository) {
        this.wishListRepository = wishListRepository;
    }
    @Override
    public WishListEntity getWishListProductById(Long productId) {
        return wishListRepository.findByProductId(productId);
    }

    @Override
    public void createWishListProduct(WishlistRequest wishlistRequest) {
        WishListEntity wishListEntity = WishListEntity.builder().productId(wishlistRequest.getProductId())
                .productName(wishlistRequest.getProductName())
                .userId(wishlistRequest.getUserId()).userName(wishlistRequest.getUserName()).build();
        wishListRepository.save(wishListEntity);
    }
}
