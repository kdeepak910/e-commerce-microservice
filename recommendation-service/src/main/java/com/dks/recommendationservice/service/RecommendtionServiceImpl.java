package com.dks.recommendationservice.service;

import com.dks.recommendationservice.entity.ProductEntity;
import com.dks.recommendationservice.entity.RecommendationEntity;
import com.dks.recommendationservice.model.FeedBackResponse;
import com.dks.recommendationservice.model.RatingResponse;
import com.dks.recommendationservice.model.RecommendationRequest;
import com.dks.recommendationservice.model.UserResponse;
import com.dks.recommendationservice.repository.RecommendationRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

@Service
public class RecommendtionServiceImpl implements RecommendtionService{

    private RecommendationRepository recommendationRepository;

    public RecommendtionServiceImpl(RecommendationRepository recommendationRepository) {
        this.recommendationRepository = recommendationRepository;
    }


    @Override
    public List<RatingResponse> getAllRecommendationByProductName(String productName) {
        List<RecommendationEntity> recommendationEntities = recommendationRepository.findAllRatingByProductName(productName);
        double averageRating = recommendationEntities.stream()
                .collect(Collectors.averagingDouble(RecommendationEntity::getRating));
        return recommendationEntities.stream().map(recommendationEntity -> ratingResponseBuilder(recommendationEntity, averageRating)).collect(Collectors.toList());

    }

    @Override
    public List<FeedBackResponse> getAllFeedBackByProductName(String productName) {
        List<RecommendationEntity> recommendationEntities = recommendationRepository.findAllRatingByProductName(productName);
        return recommendationEntities.stream().map(recommendationEntity -> feedBackResponseBuilder(recommendationEntity)).collect(Collectors.toList());
    }

    @Override
    public RecommendationEntity getRecommendationById(Long id) {
        return recommendationRepository.findById(id).get();
    }

    @Override
    @Transactional
    public RecommendationEntity saveRecommendation(RecommendationRequest recommendationRequest, ProductEntity productEntity, UserResponse userResponse) {
        RecommendationEntity recommendationEntity = RecommendationEntity.builder().productId(recommendationRequest.getProductId())
                .rating(recommendationRequest.getRating()).likeOrDislike(recommendationRequest.getLikeOrDislike())
                .userName(userResponse.getUsername()).productName(productEntity.getProductName()).feedback(recommendationRequest.getFeedback())
                .feedbackDate(recommendationRequest.getFeedbackDate()).userId(recommendationRequest.getUserId()).build();
        recommendationRepository.save(recommendationEntity);
        return recommendationEntity;
    }

    @Override
    public void deleteRecommendation(Long id) {
        recommendationRepository.deleteById(id);
    }

    private RatingResponse ratingResponseBuilder(RecommendationEntity recommendationEntity, Double averageRating) {
        return RatingResponse.builder().ratings(recommendationEntity.getRating()).averageRating(averageRating).productName(recommendationEntity.getProductName()).build();
    }

    private FeedBackResponse feedBackResponseBuilder(RecommendationEntity recommendationEntity) {
        return FeedBackResponse.builder().feedbackDate(recommendationEntity.getFeedbackDate()).feedback(recommendationEntity.getFeedback()).productName(recommendationEntity.getProductName()).build();
    }

}
