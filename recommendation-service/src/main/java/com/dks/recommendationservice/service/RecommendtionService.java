package com.dks.recommendationservice.service;

import com.dks.recommendationservice.entity.ProductEntity;
import com.dks.recommendationservice.entity.RecommendationEntity;
import com.dks.recommendationservice.model.FeedBackResponse;
import com.dks.recommendationservice.model.RatingResponse;
import com.dks.recommendationservice.model.RecommendationRequest;
import com.dks.recommendationservice.model.UserResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RecommendtionService {

    public List<RatingResponse> getAllRecommendationByProductName(String productName);

    public List<FeedBackResponse> getAllFeedBackByProductName(String productName);

    public RecommendationEntity getRecommendationById(Long id);

    public RecommendationEntity saveRecommendation(RecommendationRequest recommendationRequest, ProductEntity productEntity, UserResponse userResponse);

    public void deleteRecommendation(Long id);

}
