package com.dks.recommendationservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class RatingResponse implements Serializable {

    private static final long serialVersionUID = 10L;
    public Integer ratings;
    private String productName;
    public Double averageRating;
}
