package com.dks.recommendationservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {

    private String username;
    private String email;
    private String mobile;
    private String firstName;
    private String lastName;
    private String alternativeMobile;
    private String address;
    private String zipCode;
    private String city;
    private String country;
    private List<String> roles;
}
