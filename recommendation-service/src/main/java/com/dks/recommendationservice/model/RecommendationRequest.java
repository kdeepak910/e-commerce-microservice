package com.dks.recommendationservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class RecommendationRequest implements Serializable {

    private static final long serialVersionUID = 10L;
    private int rating;
    private Long productId;
    private Long userId;
    private String feedback;
    private LocalDate feedbackDate;
    private Boolean likeOrDislike;
}
