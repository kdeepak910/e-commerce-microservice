package com.dks.recommendationservice.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class FeedBackResponse implements Serializable {

    private static final long serialVersionUID = 10L;
    public String feedback;
    private String productName;
    private LocalDate feedbackDate;
}
