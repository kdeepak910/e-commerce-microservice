package com.dks.recommendationservice.entity;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "recommendation")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class RecommendationEntity implements Serializable {
    private static final long serialVersionUID = 10L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "rating")
    private int rating;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "product_name")
    private String productName;

    @Column (name = "user_id")
    private Long userId;

    @Column (name = "user_name")
    private String userName;

    @Column(name = "feedback")
    private String feedback;

    @Column(name = "feedback_date")
    private LocalDate feedbackDate;

    @Column(name = "like_or_dislike")
    private Boolean likeOrDislike;


}
