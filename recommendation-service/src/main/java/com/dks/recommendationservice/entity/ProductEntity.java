package com.dks.recommendationservice.entity;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "product")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="product_id", insertable = false, updatable = false, nullable = false)
    private UUID productId;

    @Column (name = "product_name")
    private String productName;

    @Column (name = "price")
    private BigDecimal price;

    @Column (name = "discription")
    private String discription;

    @Column (name = "category")
    private String category;

    @Column (name = "availability")
    private int availability;
}
