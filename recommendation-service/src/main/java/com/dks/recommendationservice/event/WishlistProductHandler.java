package com.dks.recommendationservice.event;

import com.dks.recommendationservice.messaging.MessageContainer;
import com.dks.recommendationservice.model.WishlistRequest;
import com.dks.recommendationservice.service.WishlistService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class WishlistProductHandler {

    @Autowired
    WishlistService wishlistService;

    @KafkaListener(topics = "${productservice.topic.name.consumer}",groupId = "productservice")
    public void wishListProductConsumer(ConsumerRecord<String, String> payload, Acknowledgment ack) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        MessageContainer<WishlistRequest> mc = mapper.readValue(payload.value(),new TypeReference<MessageContainer<WishlistRequest>>() {});

        WishlistRequest wishlistRequest = mc.getPayload();
        log.info("wishListProductConsumer to consume from product service to add wishList product details for product_id {} and product_name {}",
                wishlistRequest.getProductId(), wishlistRequest.getProductName());

        try {
            wishlistService.createWishListProduct(wishlistRequest);
        } catch (Exception e) {
            log.info("error :wishListProductConsumer to consume from product service to add wishList product details for product_id {} and product_name {} error is {}",
                    wishlistRequest.getProductId(), wishlistRequest.getProductName(), e.getMessage());
        }

        ack.acknowledge();
    }
}
