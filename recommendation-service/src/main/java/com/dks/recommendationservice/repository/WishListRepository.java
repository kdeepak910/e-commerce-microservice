package com.dks.recommendationservice.repository;

import com.dks.recommendationservice.entity.WishListEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WishListRepository extends JpaRepository<WishListEntity, Long> {
    WishListEntity findByProductId(Long productId);
}
