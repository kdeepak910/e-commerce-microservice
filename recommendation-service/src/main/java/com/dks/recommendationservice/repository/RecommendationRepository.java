package com.dks.recommendationservice.repository;

import com.dks.recommendationservice.entity.RecommendationEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Transactional
public interface RecommendationRepository extends JpaRepository<RecommendationEntity, Long> {

    @Query("select r FROM RecommendationEntity r WHERE r.productName = :productName")
    public List<RecommendationEntity> findAllRatingByProductName(@Param("productName") String productName);

}
