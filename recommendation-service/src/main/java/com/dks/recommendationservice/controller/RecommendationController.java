package com.dks.recommendationservice.controller;

import com.dks.recommendationservice.client.ProductClient;
import com.dks.recommendationservice.client.UserClient;
import com.dks.recommendationservice.entity.ProductEntity;
import com.dks.recommendationservice.entity.RecommendationEntity;
import com.dks.recommendationservice.http.HeaderGenerator;
import com.dks.recommendationservice.model.FeedBackResponse;
import com.dks.recommendationservice.model.RatingResponse;
import com.dks.recommendationservice.model.RecommendationRequest;
import com.dks.recommendationservice.model.UserResponse;
import com.dks.recommendationservice.service.RecommendtionService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JCircuitBreaker;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JCircuitBreakerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/recommendation")
public class RecommendationController {

    private RecommendtionService recommendationService;
    private HeaderGenerator headerGenerator;

    private final Resilience4JCircuitBreakerFactory circuitBreakerFactory;

    private ProductClient productClient;

    private UserClient userClient;


    @GetMapping("/rating/{productName}")
    public ResponseEntity<List<RatingResponse>> getAllRecommendationByProductName(@PathVariable String productName) {
        List<RatingResponse> recommendations = recommendationService.getAllRecommendationByProductName(productName);
        if(!recommendations.isEmpty()) {
            return new ResponseEntity<>(
                    recommendations,
                    headerGenerator.getHeadersForSuccessGetMethod(),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                headerGenerator.getHeadersForError(),
                HttpStatus.NOT_FOUND);
    }

    @GetMapping("/feedback/{productName}")
    public ResponseEntity<List<FeedBackResponse>> getAllFeedbacks(@PathVariable String productName) {
        List<FeedBackResponse> recommendations = recommendationService.getAllFeedBackByProductName(productName);
        if(!recommendations.isEmpty()) {
            return new ResponseEntity<>(
                    recommendations,
                    headerGenerator.getHeadersForSuccessGetMethod(),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                headerGenerator.getHeadersForError(),
                HttpStatus.NOT_FOUND);
    }

    @PostMapping("/create")
    public ResponseEntity<RecommendationEntity> saveRecommendations(@RequestBody RecommendationRequest recommendationRequest, HttpServletRequest request) {
        Resilience4JCircuitBreaker circuitBreaker = (Resilience4JCircuitBreaker) circuitBreakerFactory.create("product");
        Supplier<ProductEntity> productEntitySupplier = () -> productClient.getProductById(recommendationRequest.getProductId());
        ProductEntity productEntity = circuitBreaker.run(productEntitySupplier, throwable -> handleProductClientCase());
        Resilience4JCircuitBreaker circuitBreaker1 = (Resilience4JCircuitBreaker) circuitBreakerFactory.create("user");
        Supplier<UserResponse> userResponseSupplier = () -> userClient.getUserById(recommendationRequest.getUserId());
        UserResponse userResponse = circuitBreaker1.run(userResponseSupplier, throwable -> handleUserClientCase());
        if(Optional.ofNullable(productEntity).isPresent() && Optional.ofNullable(userResponse).isPresent()) {
            try {
                RecommendationEntity recommendation = recommendationService.saveRecommendation(recommendationRequest, productEntity, userResponse);
                return new ResponseEntity<>(
                        recommendation,
                        headerGenerator.getHeadersForSuccessPostMethod(request, recommendation.getId()),
                        HttpStatus.CREATED);
            }catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<>(
                        headerGenerator.getHeadersForError(),
                        HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(
                headerGenerator.getHeadersForError(),
                HttpStatus.BAD_REQUEST);
    }

    private ProductEntity handleProductClientCase() {
        System.out.println("fallback called product!!");
        return ProductEntity.builder().build();
    }

    private UserResponse handleUserClientCase() {
        System.out.println("fallback called user !!");
        return UserResponse.builder().build();
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<Void> deleteRecommendations(@PathVariable("id") Long id){
        RecommendationEntity recommendation = recommendationService.getRecommendationById(id);
        if(recommendation != null) {
            try {
                recommendationService.deleteRecommendation(id);
                return new ResponseEntity<>(
                        headerGenerator.getHeadersForSuccessGetMethod(),
                        HttpStatus.OK);
            }catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<>(
                        headerGenerator.getHeadersForError(),
                        HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(
                headerGenerator.getHeadersForError(),
                HttpStatus.NOT_FOUND);
    }




}
