package com.dks.recommendationservice.controller;


import com.dks.recommendationservice.entity.WishListEntity;
import com.dks.recommendationservice.http.HeaderGenerator;
import com.dks.recommendationservice.service.WishlistService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/wishlist")
public class WishlistController {

    private WishlistService wishlistService;

    private HeaderGenerator headerGenerator;

    public WishlistController(WishlistService wishlistService, HeaderGenerator headerGenerator) {
        this.wishlistService = wishlistService;
        this.headerGenerator = headerGenerator;
    }

    @GetMapping("/product/{productId}")
    public ResponseEntity<WishListEntity> getProduct(@PathVariable Long productId) {
        WishListEntity product =  wishlistService.getWishListProductById(productId);
        if(product != null) {
            return new ResponseEntity<WishListEntity>(
                    product,
                    headerGenerator.getHeadersForSuccessGetMethod(),
                    HttpStatus.OK);
        }
        return new ResponseEntity<WishListEntity>(
                headerGenerator.getHeadersForError(),
                HttpStatus.NOT_FOUND);
    }
}
