package com.dks.recommendationservice.messaging;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class MessageContainer<T> {
	private T payload;
	private String messageId;
	private String messageType;
	private String clientId;

	public MessageContainer(T payload) {
		this.setPayload(payload);
	}
}