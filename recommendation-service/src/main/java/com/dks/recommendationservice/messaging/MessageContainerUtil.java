package com.dks.recommendationservice.messaging;

public class MessageContainerUtil {

    private MessageContainerUtil(){}


    public static <T> MessageContainer<T> getPayload(T payload) {
        return new MessageContainer<>(payload);
    }
}
